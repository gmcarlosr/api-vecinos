-- MariaDB dump 10.19  Distrib 10.10.1-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: vecinos
-- ------------------------------------------------------
-- Server version	10.10.1-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `gasto`
--

DROP TABLE IF EXISTS `gasto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gasto` (
  `id_gasto` int(10) NOT NULL AUTO_INCREMENT,
  `id_tipo_gasto` int(10) NOT NULL,
  `id_periodo` int(10) NOT NULL,
  `fecha_gasto` date DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT NULL,
  `observacion` varchar(300) DEFAULT NULL,
  `usuario_actualiza` int(11) DEFAULT NULL,
  `fecha_actualiza` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_gasto`),
  KEY `fk_periodos___gastos` (`id_periodo`),
  KEY `fk_tipo_gastos___gastos` (`id_tipo_gasto`),
  CONSTRAINT `fk_periodos___gastos` FOREIGN KEY (`id_periodo`) REFERENCES `periodo` (`id_periodo`),
  CONSTRAINT `fk_tipo_gastos___gastos` FOREIGN KEY (`id_tipo_gasto`) REFERENCES `tipo_gasto` (`id_tipo_gasto`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gasto`
--

LOCK TABLES `gasto` WRITE;
/*!40000 ALTER TABLE `gasto` DISABLE KEYS */;
/*!40000 ALTER TABLE `gasto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gu_menu`
--

DROP TABLE IF EXISTS `gu_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gu_menu` (
  `id_menu` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `icono` varchar(255) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL COMMENT '0 - inactivo\n1 - activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_menu`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gu_menu`
--

LOCK TABLES `gu_menu` WRITE;
/*!40000 ALTER TABLE `gu_menu` DISABLE KEYS */;
INSERT INTO `gu_menu` VALUES
(1,'Catalogos',NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `gu_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gu_opcion`
--

DROP TABLE IF EXISTS `gu_opcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gu_opcion` (
  `id_opcion` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) DEFAULT NULL,
  `nombre` varchar(50) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `icono` varchar(255) DEFAULT NULL,
  `componente` varchar(95) DEFAULT NULL,
  `layout` varchar(45) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL COMMENT '0 - inactivo\n1 - activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_opcion`),
  KEY `fk_gu_opcion_gu_menu1_idx` (`id_menu`),
  CONSTRAINT `fk_gu_opcion_gu_menu1` FOREIGN KEY (`id_menu`) REFERENCES `gu_menu` (`id_menu`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gu_opcion`
--

LOCK TABLES `gu_opcion` WRITE;
/*!40000 ALTER TABLE `gu_opcion` DISABLE KEYS */;
INSERT INTO `gu_opcion` VALUES
(1,1,'Tablero','tablero','nc-icon nc-chart-bar-32','Dashboard','/admin',1,NULL,NULL),
(2,1,'Vecinos','vecinos','nc-icon nc-single-02','Vecinos','/admin',1,NULL,NULL),
(3,1,'Pasajes','pasajes','nc-icon nc-grid-45','Pasajes','/admin',1,NULL,NULL),
(4,1,'Pol├¡gonos','poligonos','nc-icon nc-vector','Pol├¡gonos','/admin',1,NULL,NULL),
(5,1,'Periodos','periodos','nc-icon nc-watch-time','Periodos','/admin',1,NULL,NULL),
(6,1,'Tipos de gastos','tipo_gastos','nc-icon nc-single-copy-04','TiposGastos','/admin',1,NULL,NULL),
(7,1,'Tipos de ingresos','tipo_ingresos','nc-icon nc-money-coins','TiposIngresos','/admin',1,NULL,NULL),
(8,1,'Gastos','gastos','nc-icon nc-money-coins','Gastos','/admin',1,NULL,NULL),
(9,1,'Usuarios','usuarios','nc-icon nc-money-coins','Gu_usuarios','/admin',1,NULL,NULL),
(10,1,'Roles','roles','nc-icon nc-money-coins','Gu_roles','/admin',1,NULL,NULL),
(11,1,'Menus','menus','nc-icon nc-money-coins','Gu_menus','/admin',1,NULL,NULL),
(12,1,'Opciones','opciones','nc-icon nc-money-coins','Gu_opciones','/admin',1,NULL,NULL);
/*!40000 ALTER TABLE `gu_opcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gu_rol`
--

DROP TABLE IF EXISTS `gu_rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gu_rol` (
  `id_rol` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT 0 COMMENT '0 - inactivo\n1 - activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gu_rol`
--

LOCK TABLES `gu_rol` WRITE;
/*!40000 ALTER TABLE `gu_rol` DISABLE KEYS */;
INSERT INTO `gu_rol` VALUES
(1,'admin',1,NULL,NULL);
/*!40000 ALTER TABLE `gu_rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gu_rol_opcion`
--

DROP TABLE IF EXISTS `gu_rol_opcion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gu_rol_opcion` (
  `id_rol` int(11) NOT NULL,
  `id_opcion` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_rol`,`id_opcion`),
  KEY `fk_gu_rol_opcion_gu_opcion1_idx` (`id_opcion`),
  CONSTRAINT `fk_gu_rol_opcion_gu_opcion1` FOREIGN KEY (`id_opcion`) REFERENCES `gu_opcion` (`id_opcion`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_gu_rol_opcion_gu_rol1` FOREIGN KEY (`id_rol`) REFERENCES `gu_rol` (`id_rol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gu_rol_opcion`
--

LOCK TABLES `gu_rol_opcion` WRITE;
/*!40000 ALTER TABLE `gu_rol_opcion` DISABLE KEYS */;
INSERT INTO `gu_rol_opcion` VALUES
(1,1,NULL,NULL),
(1,2,NULL,NULL),
(1,3,NULL,NULL),
(1,4,NULL,NULL),
(1,5,NULL,NULL),
(1,6,NULL,NULL),
(1,7,NULL,NULL),
(1,8,NULL,NULL),
(1,9,NULL,NULL),
(1,10,NULL,NULL),
(1,11,NULL,NULL),
(1,12,NULL,NULL);
/*!40000 ALTER TABLE `gu_rol_opcion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gu_usuario`
--

DROP TABLE IF EXISTS `gu_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gu_usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `id_rol` int(11) DEFAULT NULL,
  `usuario` varchar(50) DEFAULT NULL,
  `clave` varchar(100) DEFAULT NULL,
  `nombres` varchar(100) DEFAULT NULL,
  `apellidos` varchar(100) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `refresh_token` varchar(255) DEFAULT NULL,
  `access_token` varchar(255) DEFAULT NULL,
  `estado` tinyint(1) DEFAULT NULL COMMENT '0 - inactivo\n1 - activo',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `fk_gu_usuario_gu_rol1_idx` (`id_rol`),
  CONSTRAINT `fk_gu_usuario_gu_rol1` FOREIGN KEY (`id_rol`) REFERENCES `gu_rol` (`id_rol`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gu_usuario`
--

LOCK TABLES `gu_usuario` WRITE;
/*!40000 ALTER TABLE `gu_usuario` DISABLE KEYS */;
INSERT INTO `gu_usuario` VALUES
(1,1,'admin','admin','admin','admin',NULL,NULL,NULL,1,NULL,NULL);
/*!40000 ALTER TABLE `gu_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingreso`
--

DROP TABLE IF EXISTS `ingreso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingreso` (
  `id_ingreso` int(10) NOT NULL AUTO_INCREMENT,
  `id_periodo` int(10) NOT NULL,
  `id_vecino` int(10) NOT NULL,
  `id_tipo_ingreso` int(10) NOT NULL,
  `fecha_pago` date DEFAULT NULL,
  `monto` decimal(10,2) DEFAULT NULL,
  `usuario_actualiza` int(11) DEFAULT NULL,
  `fecha_actualiza` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_ingreso`),
  KEY `fk_periodo___ingresos` (`id_periodo`),
  KEY `fk_tipo__ingreso___ingresos` (`id_tipo_ingreso`),
  KEY `fk_vecinos___ingresos` (`id_vecino`),
  CONSTRAINT `fk_periodo___ingresos` FOREIGN KEY (`id_periodo`) REFERENCES `periodo` (`id_periodo`),
  CONSTRAINT `fk_tipo__ingreso___ingresos` FOREIGN KEY (`id_tipo_ingreso`) REFERENCES `tipo_ingreso` (`id_tipo_ingreso`),
  CONSTRAINT `fk_vecinos___ingresos` FOREIGN KEY (`id_vecino`) REFERENCES `vecino` (`id_vecino`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingreso`
--

LOCK TABLES `ingreso` WRITE;
/*!40000 ALTER TABLE `ingreso` DISABLE KEYS */;
INSERT INTO `ingreso` VALUES
(4,1,9,1,'2022-03-12',200.00,1,'2023-03-12 06:00:00');
/*!40000 ALTER TABLE `ingreso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pasaje`
--

DROP TABLE IF EXISTS `pasaje`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pasaje` (
  `id_pasaje` int(10) NOT NULL AUTO_INCREMENT,
  `nombre_pasaje` varchar(100) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `fecha_actualiza` timestamp NULL DEFAULT NULL,
  `usuario_actualiza` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_pasaje`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pasaje`
--

LOCK TABLES `pasaje` WRITE;
/*!40000 ALTER TABLE `pasaje` DISABLE KEYS */;
INSERT INTO `pasaje` VALUES
(1,'Pasaje 9','A','2023-01-08 22:41:04',1),
(2,'Pasaje 10','A','2023-01-08 22:41:04',1),
(3,'Pasaje 11','A','2023-01-08 22:41:04',1),
(4,'Pasaje 17','A','2023-01-08 22:41:04',1),
(7,'ejemplo2','A','2023-01-30 05:02:03',1),
(8,'sddsda','A','2023-01-30 04:39:04',1);
/*!40000 ALTER TABLE `pasaje` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodo`
--

DROP TABLE IF EXISTS `periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periodo` (
  `id_periodo` int(10) NOT NULL AUTO_INCREMENT,
  `descripcion_periodo` varchar(100) DEFAULT NULL,
  `fecha_inicio` date DEFAULT NULL,
  `fecha_final` date DEFAULT NULL,
  `fecha_cierre` date DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `usuario_actualiza` int(11) DEFAULT NULL,
  `fecha_actualiza` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_periodo`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodo`
--

LOCK TABLES `periodo` WRITE;
/*!40000 ALTER TABLE `periodo` DISABLE KEYS */;
INSERT INTO `periodo` VALUES
(1,'Enero/2023','2023-01-01','2023-01-31',NULL,'P',1,'2023-01-08 22:41:04'),
(2,'Marzo/2019','2022-01-09','2022-02-09',NULL,'P',1,'2022-01-09 06:00:00'),
(3,'Marzo/2023','2022-01-09','2022-02-09',NULL,'P',1,'2022-01-09 06:00:00');
/*!40000 ALTER TABLE `periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `poligono`
--

DROP TABLE IF EXISTS `poligono`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `poligono` (
  `id_poligono` int(10) NOT NULL AUTO_INCREMENT,
  `nombre_poligono` varchar(100) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `fecha_actualiza` timestamp NULL DEFAULT NULL,
  `usuario_actualiza` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_poligono`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `poligono`
--

LOCK TABLES `poligono` WRITE;
/*!40000 ALTER TABLE `poligono` DISABLE KEYS */;
INSERT INTO `poligono` VALUES
(1,'I-2','A','2023-01-08 22:41:04',1),
(2,'I-4','A','2023-01-08 22:41:04',1),
(3,'I-3','A','2023-01-08 22:41:04',1),
(4,'H-3','A','2023-01-08 22:41:04',1),
(5,'H-4','A','2023-01-08 22:41:04',1),
(6,'I-1','A','2023-01-08 22:41:04',1),
(7,'I-5','A','2023-01-30 05:02:27',1),
(12,'asss','A','2023-01-30 05:19:03',1);
/*!40000 ALTER TABLE `poligono` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_gasto`
--

DROP TABLE IF EXISTS `tipo_gasto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_gasto` (
  `id_tipo_gasto` int(10) NOT NULL AUTO_INCREMENT,
  `nombre_servicio` varchar(100) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `usuario_actualiza` int(11) DEFAULT NULL,
  `fecha_actualiza` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_tipo_gasto`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_gasto`
--

LOCK TABLES `tipo_gasto` WRITE;
/*!40000 ALTER TABLE `tipo_gasto` DISABLE KEYS */;
INSERT INTO `tipo_gasto` VALUES
(1,'VIGILANCIA','A',1,'2023-01-08 22:41:04'),
(2,'JARDINERIA','A',1,'2023-01-08 22:41:04');
/*!40000 ALTER TABLE `tipo_gasto` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_ingreso`
--

DROP TABLE IF EXISTS `tipo_ingreso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_ingreso` (
  `id_tipo_ingreso` int(10) NOT NULL AUTO_INCREMENT,
  `nombre_ingreso` varchar(100) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `usuario_actualiza` int(11) DEFAULT NULL,
  `fecha_actualiza` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id_tipo_ingreso`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_ingreso`
--

LOCK TABLES `tipo_ingreso` WRITE;
/*!40000 ALTER TABLE `tipo_ingreso` DISABLE KEYS */;
INSERT INTO `tipo_ingreso` VALUES
(1,'MANTENIMIENTO','A',1,'2023-01-08 22:41:04');
/*!40000 ALTER TABLE `tipo_ingreso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(30) DEFAULT NULL,
  `clave` varchar(45) DEFAULT NULL,
  `nombres` varchar(50) DEFAULT NULL,
  `apellidos` varchar(70) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES
(1,'admin','admin','admin','admin');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vecino`
--

DROP TABLE IF EXISTS `vecino`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vecino` (
  `id_vecino` int(10) NOT NULL AUTO_INCREMENT,
  `id_pasaje` int(10) NOT NULL,
  `id_poligono` int(10) NOT NULL,
  `nombre_completo` varchar(200) DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `fecha_pago` date DEFAULT NULL,
  `numero_casa` varchar(50) DEFAULT NULL,
  `direccion` varchar(300) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `usuario_actualiza` int(11) DEFAULT NULL,
  `fecha_actualizacion` timestamp NULL DEFAULT NULL,
  `observacion` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id_vecino`),
  KEY `fk_pasaje_r_vecinos` (`id_pasaje`),
  KEY `fk_poligono_r_vecinos` (`id_poligono`),
  CONSTRAINT `fk_pasaje_r_vecinos` FOREIGN KEY (`id_pasaje`) REFERENCES `pasaje` (`id_pasaje`),
  CONSTRAINT `fk_poligono_r_vecinos` FOREIGN KEY (`id_poligono`) REFERENCES `poligono` (`id_poligono`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vecino`
--

LOCK TABLES `vecino` WRITE;
/*!40000 ALTER TABLE `vecino` DISABLE KEYS */;
INSERT INTO `vecino` VALUES
(9,1,1,'Carlos','2020-06-01','2020-06-01','11','col x','222222222','juanperez@gmail.com','1',1,'2020-06-01 06:00:00','prueba'),
(17,1,1,'gabriel','2020-06-01','2020-06-30','777','Av. Principal 123','555-55-55','juanperez@gmail.com','1',1,'2023-01-24 17:38:57',NULL),
(19,1,2,'eeeeee','2023-01-09','2023-01-17','13','aaaa','2222222','admin@gmail.com','0',1,'2023-01-24 20:37:48','aaaaaa'),
(21,3,5,'sdsjdkjs','2023-02-06','2023-02-12','11111','col','2222222','gmcarlosrivera@gmail.com','1',1,'2023-02-08 20:32:57','aaaaa'),
(22,3,5,'sdsjdkjs','2023-02-06','2023-02-12','11111','col','2222222','gmcarlosrivera@gmail.com','1',1,'2023-02-08 20:32:57','aaaaa'),
(23,3,5,'sdsjdkjs','2023-02-06','2023-02-12','11111','col','2222222','gmcarlosrivera@gmail.com','1',1,'2023-02-08 20:32:57','aaaaa'),
(24,3,5,'sdsjdkjs','2023-02-06','2023-02-12','11111','col','2222222','gmcarlosrivera@gmail.com','1',1,'2023-02-08 20:32:57','aaaaa'),
(25,1,1,'Carlos','2020-06-01','2020-06-30','24','Av. Principal 123','555-55-55','juanperez@gmail.com','1',1,'2020-06-01 06:00:00',NULL),
(26,1,1,'Carlos','2020-06-01','2020-06-30','24','Av. Principal 123','555-55-55','juanperez@gmail.com','1',1,'2020-06-01 06:00:00',NULL),
(27,1,1,'Carlos','2020-06-01','2020-06-30','24','Av. Principal 123','555-55-55','juanperez@gmail.com','1',1,'2020-06-01 06:00:00',NULL),
(28,1,1,'Carlos','2020-06-01','2020-06-30','24','Av. Principal 123','555-55-55','juanperez@gmail.com','1',1,'2020-06-01 06:00:00',NULL),
(29,1,1,'Carlos','2020-06-01','2020-06-30','24','Av. Principal 123','555-55-55','juanperez@gmail.com','1',1,'2020-06-01 06:00:00',NULL),
(30,1,1,'Carlos','2020-06-01','2020-06-30','24','Av. Principal 123','555-55-55','juanperez@gmail.com','1',1,'2020-06-01 06:00:00',NULL),
(31,1,1,'Carlos','2020-06-01','2020-06-30','24','Av. Principal 123','555-55-55','juanperez@gmail.com','1',1,'2020-06-01 06:00:00',NULL);
/*!40000 ALTER TABLE `vecino` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-12 17:58:26
