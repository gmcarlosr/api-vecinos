//WINDOWS
//Get-ExecutionPolicy -List
//Set-ExecutionPolicy RemoteSigned -Scope CurrentUser 
//npm i pm2 -g
//pm2 start ecosystem.config.js -env production
//pm2 list
//pm2 monit
//pm2 stop ecosystem.comfig.js
module.exports = {
    apps : [{
      name: 'My App',
      script: 'index.js',
      instances: '5',
      max_memory_restart: '256',
      env: {
        NODE_ENV: 'development'
      },
      env_production: {
        NODE_ENV: 'production'
      }
    }]
  };