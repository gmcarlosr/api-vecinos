function mensaje() {
    const nombre = "Carlos"; //global
    const apellidos = "rivera";
    let resultado='hola ' + nombre + ' ' + apellidos;
    return resultado; 
}

const result=mensaje();
console.log(result);

const result2 =()=>{
    const nombre = "Carlos"; //global
    const apellidos = "rivera";
    let resultado='hola2 ' + nombre + ' ' + apellidos;
    return resultado;
}
console.log(result2());

(()=>{
    const nombre = "Carlos"; //global
    const apellidos = "rivera";
    let resultado='hola3 ' + nombre + ' ' + apellidos;
    console.log(resultado);
})();


//otros snippets
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

//log winston y morgan
const morgan = require('morgan');
const winston = require('winston');
/* app.use(require('morgan')('combined', {
    stream: winston.stream,
    skip: (req, res) => res.statusCode < 400
  }));

winston.add(new winston.transports.File({ filename: 'logs/combined.log' })); */