# GitLab Clonar 

GitLab es una herramienta de control de versiones gratuita, que te permite controlar tu código fuente y el desarrollo de tus proyectos. Esta guía te explicará cómo clonar un repositorio de GitLab.

## Pre-requisitos 

* Una cuenta de GitLab.
* Instalar [Git](https://git-scm.com/) en tu computadora.

## Clonar un repositorio

1. Inicia sesión en tu cuenta de GitLab.
2. Copia la URL SSH del repositorio que quieres clonar. En este caso, la URL es `git@gitlab.com:gmcarlosr/api-vecinos.git`.
3. Abre tu terminal y navega hasta la carpeta donde quieres clonar el repositorio.
4. Ejecuta el comando `git clone git@gitlab.com:gmcarlosr/api-vecinos.git`. Esto creará una carpeta con el mismo nombre de tu repositorio y clonará todos los archivos en ella.

## Instalación de npm i

1. Para instalar el paquete NPM, primero necesitas navegar hasta la carpeta del repositorio que clonaste previamente.
2. Una vez allí, ejecuta el comando `npm i`.
3. Esto leerá el archivo `package.json` y descargará e instalará todas las dependencias necesarias para que el proyecto funcione correctamente.

## Branches

1. El repositorio incluye dos ramas: la rama principal (`main`) y la rama de desarrollo (`development`).
2. Recuerda que no debes hacer `push` a ninguna rama.

## Archivos adicionales

Este repositorio incluye también un archivo `.sql` de prueba, un archivo `.env-example` para configurar las variables de entorno necesarias para el proyecto y una colección de Postman para probar la API.