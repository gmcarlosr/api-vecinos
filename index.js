const express = require("express");
var cors =require('cors');
require("dotenv").config();
var config = require("./dbconfig.js");
const mariadb = require("mariadb");
const routerApi = require('./routes');
//const test = require('./test.js');
const pool = mariadb.createPool(config);
const app = express();
app.use(cors());
app.use(express.json()); // sirve para enviar informacion en formato json
routerApi(app);
app.listen(4000);

console.log(`Server listen 4000`);

