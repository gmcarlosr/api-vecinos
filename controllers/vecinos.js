const config = require("../dbconfig.js");
const mariadb = require("mariadb");
const pool = mariadb.createPool(config);
const { json } = require("express");

async function obtenerVecinos(req, res) {
    try {
        const conn = await pool.getConnection();
        const query = "SELECT * FROM vecino";
        const data = await conn.query(query);
        conn.end(); // cerrar el pool de conexion
        if (data.length == 0) {
            res.status(404).json([]);
        } else {
            res.status(200).json(data);
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({ message: "Error" });
    }
}

async function obtenerVecinosPaginacion(req, res) {
    try {
        const { offset, limit, search } = req.query;
        const values = [parseInt(limit), parseInt(offset)]; //limit y offset paginar
        const values2 = []; // no paginar y contar todos los registros

        let query = "";
        let query2 = "";

        if (search) {
            query = "SELECT * FROM vecino WHERE nombre_completo LIKE ? OR fecha_pago LIKE ? OR fecha_ingreso LIKE ? LIMIT ? OFFSET ?";
            values.unshift(`%${search}%`);
            values.unshift(`%${search}%`);
            values.unshift(`%${search}%`);

            query2 = "SELECT count(1) as cant FROM vecino WHERE nombre_completo LIKE ? OR fecha_pago LIKE ? OR fecha_ingreso LIKE ? ";
            values2.unshift(`%${search}%`);
            values2.unshift(`%${search}%`);
            values2.unshift(`%${search}%`);

        } else {
            query = "SELECT * FROM vecino LIMIT ? OFFSET ?";
            query2 = "SELECT count(1) as cant FROM vecino";
        }

        const conn = await pool.getConnection();
        const data = await conn.query(query, values);
        const cantidad = await conn.query(query2, values2);

        const response = {
            data: data,
            cantidad: cantidad[0].cant.toString()
        }

        conn.end();
        if (data.length == 0) {
            res.status(404).json([]);
        } else {
            res.status(200).json(response);
        }

    } catch (e) {
        console.log(e);
        res.status(500).json({ message: "Error" });
    }
}



module.exports = {
    obtenerVecinos: obtenerVecinos,
    obtenerVecinosPaginacion: obtenerVecinosPaginacion
}

