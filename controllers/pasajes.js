const config = require("../dbconfig.js");
const mariadb = require("mariadb");
const pool = mariadb.createPool(config);
const { json } = require("express");

 async (req, res) => {
    try{
      const conn = await pool.getConnection();
      const query = "SELECT * FROM pasaje";
      const pasaje = await conn.query(query);
      conn.end();
  
      if(pasaje.length === 0){
        res.json([]);
      }
      else{
        res.status(200).json(pasaje);
      }
    }
    catch(e){
      console.log(e);
      res.status(500).json({message:"Error"});
    }
  };

 async (req, res) => {
    
  try {
    const {offset,limit,search} = req.query;
    const values = [parseInt(limit),parseInt(offset)];
    const values2 = [];
    let query2="";
    let query = "";
    if (search) {
      query = "SELECT * FROM pasaje WHERE nombre_pasaje LIKE ? OR if(estado='A','ACTIVO','INACTIVO') LIKE ? LIMIT ? OFFSET ?";
      values.unshift(`%${search}%`);
      values.unshift(`%${search}%`);
      query2 = "SELECT count(1) as cant FROM pasaje WHERE nombre_pasaje LIKE ? OR if(estado='A','ACTIVO','INACTIVO') LIKE ?";
      values2.unshift(`%${search}%`);
      values2.unshift(`%${search}%`);
    } else {
      query = "SELECT * FROM pasaje LIMIT ? OFFSET ?";
      query2 = "SELECT count(1) as cant FROM pasaje";
      
    }
    const conn = await pool.getConnection();
    const data = await conn.query(query,values);
    const cantidad = await conn.query(query2,values2);

    
    const response = {
      data: data,
      cantidad: cantidad[0].cant.toString()
    }
    conn.end();
    if (data.length === 0) {
        res.status(404).json([]);   
    } else {
        res.status(200).json(response);
    }
} catch (e) {
    console.log(e); //servidor
    res.status(500).json({ message: "Error" }); //navegador o cliente
}
};
  
  //Mostrar un solo pasaje
  async(req, res) =>{
    try{
      const conn = await pool.getConnection();
      const { id } = req.params;
      const query =  "SELECT * FROM pasaje WHERE id_pasaje = ?";
      const pasaje =  await conn.query(query, [id]);
      conn.end();
  
      if(pasaje.length === 0){
        res.status(404).json({});
      }
      else{
        res.status(200).json(pasaje);
      }
    }
    catch(e){
      console.log(e);
      res.status(500).json({message:"Error"});
    }
  };
  
  //Eliminar un pasaje
  async(req, res) => {
    try{
      const conn = await pool.getConnection();
      const { id } = req.params;
      const query = "SELECT * FROM pasaje WHERE id_pasaje = ? ";
      const pasaje = await conn.query(query, [id]);
      if(pasaje.length === 0){
        res.status(404).json({message:"Pasaje no encontrado"});
        conn.end();
      }
      else{
        await conn.query("DELETE FROM pasaje WHERE id_pasaje =?", [id]);
        conn.end();
        res.status(200).json({message:"Pasaje eliminado exitosamente"});
      }
    }
    catch(e){
      console.log(e);
      res.status(500).json({message:"Error"});
    }
  };
  
  //Agregar un pasaje
  async(req, res) => {
    const conn = await pool.getConnection();
    const {
      nombre_pasaje,
      estado,
      usuario_actualiza,
      fecha_actualiza
    } = req.body;
  
    if(nombre_pasaje == undefined || nombre_pasaje == null || nombre_pasaje === ""){
      res.status(400).json({message:"Error: El nombre del pasaje no puede estar vacía"});
    }
    if(estado == undefined || estado == null || estado === ""){
      res.status(400).json({message:"Error: El estado no puede estar vacío"});
    }
    if(fecha_actualiza == undefined || fecha_actualiza == null || fecha_actualiza === ""){
      res.status(400).json({message:"Error: La fecha no puede estar vacía"});
    }
    if(usuario_actualiza == undefined || usuario_actualiza == null || usuario_actualiza === ""){
      res.status(400).json({message:"Error: El usuario que actualiza no puede estar vacío"});
    }
  
    const query = "INSERT INTO pasaje (nombre_pasaje, estado, fecha_actualiza, usuario_actualiza) VALUES (?,?,?,?) ";
    try{
      await conn.query(query, [
        nombre_pasaje,
        estado,
        fecha_actualiza,
        usuario_actualiza
      ]);
      conn.end();
      res.status(201).json({message:"El pasaje ha sido agregado exitosamente"});
    }
    catch(e){
      console.log(e);
      res.status(404).json({message:"Error"});
    }
  };
  
  //Actualizar un pasaje
  async (req, res) => {
    const conn = await pool.getConnection();
    const { id } = req.params;
    const {
      nombre_pasaje,
      estado,
      usuario_actualiza,
      fecha_actualiza
    } = req.query;
    let query = "UPDATE pasaje SET ";
    const values = [];
    if (nombre_pasaje) {
      query += "nombre_pasaje = ?, ";
      values.push(nombre_pasaje);
    }
    if (estado) {
      query += "estado = ?, ";
      values.push(estado);
    }
    if (usuario_actualiza) {
      query += "usuario_actualiza = ?, ";
      values.push(usuario_actualiza);
    }
    if (fecha_actualiza) {
      query += "fecha_actualiza = ?, ";
      values.push(fecha_actualiza);
    }
    query = query.slice(0, -2) + " WHERE id_pasaje = ?";
    values.push(id);
  
    try {
      await conn.query(query, values);
      conn.end();
      res.status(200).json({ message: "Pasaje actualizado exitosamente" });
    } catch (e) {
      console.log(e);
      res.status(500).json({ message:" Error "});
    }
  };

  module.exports = router;