//Helpers
const { validateResult } = require('../helpers/validateHelper');

//Module validator
const { check } = require('express-validator');

//List errors
const { errorsCode } = require('./errors');


const validateCreate = [
    check('nombre_completo').exists().withMessage(errorsCode[8].msg).isLength({ min: 1, max: 255 }).not().isEmpty().withMessage(errorsCode[0].msg).isLength({ min: 1, max: 255 }).withMessage(errorsCode[3].msg + "min:1 max:255"),
    check('numero_casa').exists().not().isEmpty().withMessage(errorsCode[0].msg).isLength({ min: 1, max: 20 }).withMessage(errorsCode[3].msg + "min:1 max:20"),
    check('id_poligono').exists().not().isEmpty().withMessage(errorsCode[0].msg).isInt().withMessage(errorsCode[1].msg),
    check('id_pasaje').exists().not().isEmpty().withMessage(errorsCode[0].msg).isInt().withMessage(errorsCode[1].msg),
    check('fecha_pago').exists().not().isEmpty().withMessage(errorsCode[0].msg).isISO8601().toDate().withMessage(errorsCode[4].msg),
    check('direccion').exists().not().isEmpty().withMessage(errorsCode[0].msg).isLength({ min: 1, max: 255 }).withMessage(errorsCode[3].msg + "min:1 max:255"),
    check('estado').exists().not().isEmpty().withMessage(errorsCode[0].msg).isInt().withMessage(errorsCode[1].msg).custom(value => {
        if (value < 0 || value > 1) {
            throw new Error(errorsCode[2].msg + "0 y 1");
        }
        return true;
    }),
    check('usuario_actualiza').exists().not().isEmpty().withMessage(errorsCode[0].msg).isInt().withMessage(errorsCode[1].msg),
    check('fecha_actualizacion').exists().not().isEmpty().withMessage(errorsCode[0].msg).isISO8601().toDate().withMessage(errorsCode[4].msg),
    check('telefono').optional().matches(/^\d{4}\-\d{4}$/).withMessage(errorsCode[5].msg),
    check('email').optional().isEmail().withMessage(errorsCode[6].msg),
    check('observacion').optional().isLength({ min: 1, max: 500 }).withMessage(errorsCode[3].msg + "min:1 max:500"),
    (req, res, next) => {
        validateResult(req, res, next);
    }

];

const validateUpdate = [
    check('nombre_completo').optional().isLength({ min: 1, max: 255 }).withMessage(errorsCode[3].msg + "min:1 max:255"),
    check('numero_casa').optional().isLength({ min: 1, max: 20 }).withMessage(errorsCode[3].msg + "min:1 max:20"),
    check('id_poligono').optional().isInt().withMessage(errorsCode[1].msg),
    check('fecha_pago').optional().isISO8601().toDate().withMessage(errorsCode[4].msg),
    check('direccion').optional().isLength({ min: 1, max: 255 }).withMessage(errorsCode[3].msg + "min:1 max:255"),
    check('estado').optional().isInt().withMessage(errorsCode[1].msg).custom(value => {
        if (value < 0 || value > 1) {
            throw new Error(errorsCode[2].msg + "0 y 1");
        }
        return true;
    }),
    check('usuario_actualiza').optional().isInt().withMessage(errorsCode[1].msg),
    check('fecha_actualizacion').optional().isISO8601().toDate().withMessage(errorsCode[4].msg),
    check('telefono').optional().matches(/^\d{4}\-\d{4}$/).withMessage(errorsCode[5].msg),
    check('email').optional().isEmail().withMessage(errorsCode[6].msg),
    check('observacion').optional().isLength({ min: 1, max: 500 }).withMessage(errorsCode[3].msg + "min:1 max:500"),
    (req, res, next) => {
        if (Object.keys(req.body) != 0) {
            validateResult(req, res, next);
        } else {
            res.status(400).json({
                errors: errorsCode[6].msg
            })
        }
    }
];

module.exports = { validateCreate, validateUpdate };