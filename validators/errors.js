const errorsCode = [
    {
        code: 0,
        msg: 'Campo no puede ser vacio'
    },
    {
        code: 1,
        msg: 'Campo debe ser entero'
    },
    {
        code: 2,
        msg: 'Campo debe ser entero entre el rango de: '
    },
    {
        code: 3,
        msg: 'Campo debe cumplir el minimo y maximo de caracteres de: '
    },
    {
        code: 4,
        msg: 'Campo de fecha invalido'
    },
    {
        code: 5,
        msg: 'Campo de numero telefonico invalido'
    },
    {
        code: 6,
        msg: 'Campo de tipo email invalido'
    },
    {
        code: 7,
        msg: 'Al menos un parametro debe ser enviado'
    },
    {
        code: 8,
        msg: 'Campo requerido'
    }




];

module.exports = { errorsCode };