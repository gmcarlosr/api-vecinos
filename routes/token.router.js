const express = require('express');
const router = express.Router();
var config = require("../dbconfig.js");
const mariadb = require("mariadb");
const pool = mariadb.createPool(config);

const jwt = require('jsonwebtoken'); //jwt
const bcrypt = require('bcrypt'); //hash refresh token
const SECRET_KEY= process.env.SECRET_KEY; //firma

router.get('/', async (req, res) => {
    const conn = await pool.getConnection();
    const {usuario,clave} = req.query;
    if(usuario === ""){
      res.json({ message: `Error: El usuario no puede estar vacío` });
      return;
    }
    if(clave === ""){
      res.json({ message: `Error: La clave no puede estar vacío` });
      return;
    }
    const query =  `SELECT id_usuario,id_rol,usuario FROM gu_usuario WHERE usuario = ? AND clave = ?`;
    try {
      const rows = await conn.query(query, [usuario,clave]);
      conn.end();
      if (rows.length > 0) {
        const userData=rows[0];
        const accessToken = jwt.sign({ usuario, userData}, SECRET_KEY, { expiresIn: '8h' }); // creacion del jwt
        const refreshToken = await bcrypt.hash(usuario, 10);
        res.json({ accessToken, refreshToken,userData});
      } else {
        res.status(400).json({ error: 'Invalid Credentials' });
      }
    } catch (e) {
      console.log(e);
      res.json({ message: `Error` });
    }
  });

  router.post('/refresh', (req, res) => {
    const { refreshToken, usuario} = req.body;
    if (bcrypt.compare(usuario, refreshToken)) {
      const accessToken = jwt.sign({ usuario }, SECRET_KEY, { expiresIn: '8h' });
      res.json({ accessToken });
    } else {
      res.sendStatus(401);
    }
  });


  module.exports = router;