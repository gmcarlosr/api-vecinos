const express = require('express');
const vecinoRouter = require('./vecinos.router');
const tokensRouter = require('./token.router');

function routerApi(app){
    const router = express.Router();
    app.use('/api/v1',router);
    router.use('/vecinos',vecinoRouter);
    router.use('/token',tokensRouter);
}

module.exports = routerApi;