const express = require('express');
const router = express.Router();
var config = require("../dbconfig.js");
const mariadb = require("mariadb");
const pool = mariadb.createPool(config);
//const test = require('../test');
//Middlewares
const verifyToken = require('../middlewares/verifyToken');
const {validateCreate,validateUpdate} = require('../validators/vecinos');
const vecinoController= require("../controllers/vecinos.js")


//Mostrar todos los vecinos
router.get('/', async (req, res) => {
   vecinoController.obtenerVecinos(req,res);
});

//Mostrar todos los vecinos con paginacion
router.get('/paginacion', async (req, res) => {
   vecinoController.obtenerVecinosPaginacion(req,res);
});

//ejemplo de procedure
router.get("/ejemplo/:id", verifyToken, async (req, res) => {
    try{
      const conn = await pool.getConnection();
      const {id} = req.params;
      const query =  "CALL proc_list_cargos_por_vecino(?)" ;
      const [data] = await conn.query(query,[id]);
      conn.end();
  
      const response = {
        data: data,
      }
      if(data.length === 0){
        res.json([]);
      }
      else{
        res.status(200).json(response);
      }
    }
    catch(e){
      console.log(e);
      res.status(500).json({message:"Error"});
    }
  });

//Mostrar un vecino
router.get('/:id', verifyToken, async (req, res) => {
    try {
        const { id } = req.params; // params o params uri
        const conn = await pool.getConnection();
        const query = "SELECT * FROM vecino WHERE id_vecino=?";
        const data = await conn.query(query, [id]);
        conn.end();

        if (data.length == 0) {
            res.status(404).json([]);
        } else {
            res.status(200).json(data);
        }

    } catch (e) {
        console.log(e);
        res.status(500).json({ message: "Error" });
    }
});

//Eliminar un vecino
router.delete('/:id', async (req, res) => {
    try {
        const { id } = req.params;
        const conn = await pool.getConnection();

        // Primero comprobamos si hay registros relacionados en la tabla de ingresos
        const queryIngresos = "SELECT * FROM ingreso WHERE id_vecino=?";
        const dataIngresos = await conn.query(queryIngresos, [id]);

        if (dataIngresos.length > 0) {
            // Si hay registros relacionados, no podemos eliminar el vecino
            res.status(400).json({ message: 'No se puede eliminar el vecino porque hay registros relacionados en la tabla de ingresos' });
        } else {
            // Si no hay registros relacionados, entonces eliminamos el vecino
            const queryVecinos = "DELETE FROM vecino WHERE id_vecino=?";
            const data = await conn.query(queryVecinos, [id]);
            conn.end();

            if (data.affectedRows == 0) {
                res.status(404).json({ message: 'Ningun registro afectado' });
            } else {
                res.status(200).json({ message: 'Eliminado exitosamente' });
            }
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({ message: "Error" });
    }
});

//Agregar un vecino
router.post("/", verifyToken,validateCreate, async (req, res) => {
    const conn = await pool.getConnection();
    const {
        id_pasaje,
        id_poligono,
        nombre_completo,
        fecha_ingreso,
        fecha_pago,
        numero_casa,
        direccion,
        telefono,
        email,
        estado,
        usuario_actualiza,
        fecha_actualizacion,
        observacion
    } = req.body;

    const query =
        `INSERT INTO vecino (id_pasaje,id_poligono,nombre_completo,fecha_ingreso,fecha_pago,
        numero_casa,direccion,telefono,email,estado,usuario_actualiza,fecha_actualizacion,observacion) 
        VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)`;
    try {
        const data = await conn.query(query, [
            id_pasaje,
            id_poligono,
            nombre_completo,
            fecha_ingreso,
            fecha_pago,
            numero_casa,
            direccion,
            telefono || null,
            email || null,
            estado,
            usuario_actualiza,
            fecha_actualizacion,
            observacion || null
        ]);
        conn.end();
        if (data.affectedRows == 0) {
            res.status(404).json({ message: 'Ningun registro afectado' });
        } else {
            res.status(201).json({ message: `Registro insertado exitosamente` });
        }
    } catch (e) {
        console.log(e);
        res.status(500).json({ message: `Error` });
    }
});

//Actualizar un vecino
router.put("/:id", verifyToken,validateUpdate, async (req, res) => {
    const conn = await pool.getConnection();
    const { id } = req.params;
    const {
        nombre_completo,
        fecha_ingreso,
        fecha_pago,
        numero_casa,
        direccion,
        telefono,
        email,
        estado,
        usuario_actualiza,
        fecha_actualizacion,
        observacion
    } = req.body;

    let query = "UPDATE vecino SET ";
    const values = [];
    if (nombre_completo) {
        query += "nombre_completo = ?, ";
        values.push(nombre_completo);
    }
    if (fecha_ingreso) {
        query += "fecha_ingreso = ?, ";
        values.push(fecha_ingreso);
    }
    if (fecha_pago) {
        query += "fecha_pago = ?, ";
        values.push(fecha_pago);
    }
    if (numero_casa) {
        query += "numero_casa = ?, ";
        values.push(numero_casa);
    }
    if (direccion) {
        query += "direccion = ?, ";
        values.push(direccion);
    }
    if (telefono) {
        query += "telefono = ?, ";
        values.push(telefono);
    }
    if (email) {
        query += "email = ?, ";
        values.push(email);
    }
    if (estado) {
        query += "estado = ?, ";
        values.push(estado);
    }
    if (usuario_actualiza) {
        query += "usuario_actualiza = ?, ";
        values.push(usuario_actualiza);
    }
    if (fecha_actualizacion) {
        query += "fecha_actualizacion = ?, ";
        values.push(fecha_actualizacion);
    }
    if (observacion) {
        query += "observacion = ?, ";
        values.push(observacion);
    }
    query = query.slice(0, -2) + " WHERE id_vecino = ?";
    values.push(id);

    try {
        const data = await conn.query(query, values);
        conn.end();
        if (data.affectedRows == 0) {
            res.status(404).json({ message: 'Ningun registro afectado' });
        } else {
            res.status(200).json({ message: `Registro actualizado exitosamente` });
        }

    } catch (e) {
        console.log(e);
        res.status(500).json({ message: `Error` });
    }

});

module.exports = router;